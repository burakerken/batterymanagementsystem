_gonder:
;Node_1.c,38 :: 		void gonder()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Node_1.c,42 :: 		UART1_Write_Text("B1 123");UART1_Write(x[0]);UART1_Write_Text("567890");
MOVW	R0, #lo_addr(?lstr3_Node_1+0)
MOVT	R0, #hi_addr(?lstr3_Node_1+0)
BL	_UART1_Write_Text+0
MOVW	R0, #lo_addr(_x+0)
MOVT	R0, #hi_addr(_x+0)
LDRB	R0, [R0, #0]
BL	_UART1_Write+0
MOVW	R0, #lo_addr(?lstr4_Node_1+0)
MOVT	R0, #hi_addr(?lstr4_Node_1+0)
BL	_UART1_Write_Text+0
;Node_1.c,43 :: 		UART1_Write(79);
MOVS	R0, #79
BL	_UART1_Write+0
;Node_1.c,44 :: 		UART1_Write(75);
MOVS	R0, #75
BL	_UART1_Write+0
;Node_1.c,46 :: 		delay_ms(200);
MOVW	R7, #40703
MOVT	R7, #36
NOP
NOP
L_gonder0:
SUBS	R7, R7, #1
BNE	L_gonder0
NOP
NOP
NOP
;Node_1.c,48 :: 		UART1_Write_Text("B2 123");UART1_Write(x[1]);UART1_Write_Text("567890");
MOVW	R0, #lo_addr(?lstr5_Node_1+0)
MOVT	R0, #hi_addr(?lstr5_Node_1+0)
BL	_UART1_Write_Text+0
MOVW	R0, #lo_addr(_x+1)
MOVT	R0, #hi_addr(_x+1)
LDRB	R0, [R0, #0]
BL	_UART1_Write+0
MOVW	R0, #lo_addr(?lstr6_Node_1+0)
MOVT	R0, #hi_addr(?lstr6_Node_1+0)
BL	_UART1_Write_Text+0
;Node_1.c,50 :: 		UART1_Write(79);
MOVS	R0, #79
BL	_UART1_Write+0
;Node_1.c,51 :: 		UART1_Write(75);
MOVS	R0, #75
BL	_UART1_Write+0
;Node_1.c,53 :: 		delay_ms(200);
MOVW	R7, #40703
MOVT	R7, #36
NOP
NOP
L_gonder2:
SUBS	R7, R7, #1
BNE	L_gonder2
NOP
NOP
NOP
;Node_1.c,55 :: 		UART1_Write_Text("B3 123");UART1_Write(x[2]);UART1_Write_Text("567890");
MOVW	R0, #lo_addr(?lstr7_Node_1+0)
MOVT	R0, #hi_addr(?lstr7_Node_1+0)
BL	_UART1_Write_Text+0
MOVW	R0, #lo_addr(_x+2)
MOVT	R0, #hi_addr(_x+2)
LDRB	R0, [R0, #0]
BL	_UART1_Write+0
MOVW	R0, #lo_addr(?lstr8_Node_1+0)
MOVT	R0, #hi_addr(?lstr8_Node_1+0)
BL	_UART1_Write_Text+0
;Node_1.c,56 :: 		UART1_Write(79);
MOVS	R0, #79
BL	_UART1_Write+0
;Node_1.c,57 :: 		UART1_Write(75);
MOVS	R0, #75
BL	_UART1_Write+0
;Node_1.c,60 :: 		delay_ms(200);
MOVW	R7, #40703
MOVT	R7, #36
NOP
NOP
L_gonder4:
SUBS	R7, R7, #1
BNE	L_gonder4
NOP
NOP
NOP
;Node_1.c,62 :: 		UART1_Write_Text("B4 123");UART1_Write(x[3]);UART1_Write_Text("567890");
MOVW	R0, #lo_addr(?lstr9_Node_1+0)
MOVT	R0, #hi_addr(?lstr9_Node_1+0)
BL	_UART1_Write_Text+0
MOVW	R0, #lo_addr(_x+3)
MOVT	R0, #hi_addr(_x+3)
LDRB	R0, [R0, #0]
BL	_UART1_Write+0
MOVW	R0, #lo_addr(?lstr10_Node_1+0)
MOVT	R0, #hi_addr(?lstr10_Node_1+0)
BL	_UART1_Write_Text+0
;Node_1.c,64 :: 		UART1_Write(79);
MOVS	R0, #79
BL	_UART1_Write+0
;Node_1.c,66 :: 		UART1_Write(75);
MOVS	R0, #75
BL	_UART1_Write+0
;Node_1.c,68 :: 		GPIOC_ODR.B13=1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOC_ODR+0)
MOVT	R0, #hi_addr(GPIOC_ODR+0)
STR	R1, [R0, #0]
;Node_1.c,70 :: 		delay_ms(100);
MOVW	R7, #20351
MOVT	R7, #18
NOP
NOP
L_gonder6:
SUBS	R7, R7, #1
BNE	L_gonder6
NOP
NOP
NOP
;Node_1.c,72 :: 		GPIOC_ODR.B13=0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOC_ODR+0)
MOVT	R0, #hi_addr(GPIOC_ODR+0)
STR	R1, [R0, #0]
;Node_1.c,74 :: 		delay_ms(100);
MOVW	R7, #20351
MOVT	R7, #18
NOP
NOP
L_gonder8:
SUBS	R7, R7, #1
BNE	L_gonder8
NOP
NOP
NOP
;Node_1.c,78 :: 		}
L_end_gonder:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _gonder
_max_detect:
;Node_1.c,80 :: 		void max_detect()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Node_1.c,82 :: 		maximum=gelen_adc_float1;
MOVW	R2, #lo_addr(_gelen_adc_float1+0)
MOVT	R2, #hi_addr(_gelen_adc_float1+0)
LDR	R1, [R2, #0]
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
STR	R1, [R0, #0]
;Node_1.c,83 :: 		if(gelen_adc_float2>=maximum)
MOV	R0, R2
LDR	R2, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc_float2+0)
MOVT	R0, #hi_addr(_gelen_adc_float2+0)
LDR	R0, [R0, #0]
BL	__Compare_FP+0
MOVW	R0, #0
BLT	L__max_detect61
MOVS	R0, #1
L__max_detect61:
CMP	R0, #0
IT	EQ
BEQ	L_max_detect10
;Node_1.c,84 :: 		maximum=gelen_adc_float2;
MOVW	R0, #lo_addr(_gelen_adc_float2+0)
MOVT	R0, #hi_addr(_gelen_adc_float2+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
STR	R1, [R0, #0]
IT	AL
BAL	L_max_detect11
L_max_detect10:
;Node_1.c,85 :: 		else if(gelen_adc_float3>=maximum)
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
LDR	R2, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc_float3+0)
MOVT	R0, #hi_addr(_gelen_adc_float3+0)
LDR	R0, [R0, #0]
BL	__Compare_FP+0
MOVW	R0, #0
BLT	L__max_detect62
MOVS	R0, #1
L__max_detect62:
CMP	R0, #0
IT	EQ
BEQ	L_max_detect12
;Node_1.c,86 :: 		maximum=gelen_adc_float3;
MOVW	R0, #lo_addr(_gelen_adc_float3+0)
MOVT	R0, #hi_addr(_gelen_adc_float3+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
STR	R1, [R0, #0]
IT	AL
BAL	L_max_detect13
L_max_detect12:
;Node_1.c,87 :: 		else if(gelen_adc_float4>=maximum)
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
LDR	R2, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc_float4+0)
MOVT	R0, #hi_addr(_gelen_adc_float4+0)
LDR	R0, [R0, #0]
BL	__Compare_FP+0
MOVW	R0, #0
BLT	L__max_detect63
MOVS	R0, #1
L__max_detect63:
CMP	R0, #0
IT	EQ
BEQ	L_max_detect14
;Node_1.c,88 :: 		maximum=gelen_adc_float4;
MOVW	R0, #lo_addr(_gelen_adc_float4+0)
MOVT	R0, #hi_addr(_gelen_adc_float4+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
STR	R1, [R0, #0]
L_max_detect14:
L_max_detect13:
L_max_detect11:
;Node_1.c,91 :: 		}
L_end_max_detect:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _max_detect
_min_detect:
;Node_1.c,93 :: 		void min_detect()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Node_1.c,95 :: 		minimum=gelen_adc_float1;
MOVW	R2, #lo_addr(_gelen_adc_float1+0)
MOVT	R2, #hi_addr(_gelen_adc_float1+0)
LDR	R1, [R2, #0]
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
STR	R1, [R0, #0]
;Node_1.c,96 :: 		if(gelen_adc_float2<=minimum)
MOV	R0, R2
LDR	R2, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc_float2+0)
MOVT	R0, #hi_addr(_gelen_adc_float2+0)
LDR	R0, [R0, #0]
BL	__Compare_FP+0
MOVW	R0, #0
BGT	L__min_detect65
MOVS	R0, #1
L__min_detect65:
CMP	R0, #0
IT	EQ
BEQ	L_min_detect15
;Node_1.c,97 :: 		minimum=gelen_adc_float2;
MOVW	R0, #lo_addr(_gelen_adc_float2+0)
MOVT	R0, #hi_addr(_gelen_adc_float2+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
STR	R1, [R0, #0]
IT	AL
BAL	L_min_detect16
L_min_detect15:
;Node_1.c,98 :: 		else if(gelen_adc_float3<=minimum)
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
LDR	R2, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc_float3+0)
MOVT	R0, #hi_addr(_gelen_adc_float3+0)
LDR	R0, [R0, #0]
BL	__Compare_FP+0
MOVW	R0, #0
BGT	L__min_detect66
MOVS	R0, #1
L__min_detect66:
CMP	R0, #0
IT	EQ
BEQ	L_min_detect17
;Node_1.c,99 :: 		minimum=gelen_adc_float3;
MOVW	R0, #lo_addr(_gelen_adc_float3+0)
MOVT	R0, #hi_addr(_gelen_adc_float3+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
STR	R1, [R0, #0]
IT	AL
BAL	L_min_detect18
L_min_detect17:
;Node_1.c,100 :: 		else if(gelen_adc_float4<=minimum)
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
LDR	R2, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc_float4+0)
MOVT	R0, #hi_addr(_gelen_adc_float4+0)
LDR	R0, [R0, #0]
BL	__Compare_FP+0
MOVW	R0, #0
BGT	L__min_detect67
MOVS	R0, #1
L__min_detect67:
CMP	R0, #0
IT	EQ
BEQ	L_min_detect19
;Node_1.c,101 :: 		minimum=gelen_adc_float4;
MOVW	R0, #lo_addr(_gelen_adc_float4+0)
MOVT	R0, #hi_addr(_gelen_adc_float4+0)
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
STR	R1, [R0, #0]
L_min_detect19:
L_min_detect18:
L_min_detect16:
;Node_1.c,102 :: 		}
L_end_min_detect:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _min_detect
_DSF:
;Node_1.c,107 :: 		void DSF() iv IVT_INT_USART1 ics ICS_AUTO
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Node_1.c,109 :: 		if (UART1_Data_Ready() == 1)
BL	_UART1_Data_Ready+0
CMP	R0, #1
IT	NE
BNE	L_DSF20
;Node_1.c,111 :: 		UART1_Read_Text(vassaf, "OK", 50);    // reads text until 'OK' is found
MOVW	R1, #lo_addr(?lstr11_Node_1+0)
MOVT	R1, #hi_addr(?lstr11_Node_1+0)
MOVW	R0, #lo_addr(_vassaf+0)
MOVT	R0, #hi_addr(_vassaf+0)
LDR	R0, [R0, #0]
MOVS	R2, #50
BL	_UART1_Read_Text+0
;Node_1.c,118 :: 		if(vassaf[1]=='1')
MOVW	R0, #lo_addr(_vassaf+0)
MOVT	R0, #hi_addr(_vassaf+0)
LDR	R0, [R0, #0]
ADDS	R0, R0, #1
LDRB	R0, [R0, #0]
CMP	R0, #49
IT	NE
BNE	L_DSF21
;Node_1.c,119 :: 		{gelen_adc=vassaf[3];gelen_akim=vassaf[4];deger=vassaf[6]; if((gelen_akim>=230)||(gelen_akim<=20))gelen_akim=0;
MOVW	R3, #lo_addr(_vassaf+0)
MOVT	R3, #hi_addr(_vassaf+0)
LDR	R0, [R3, #0]
ADDS	R0, R0, #3
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
STRH	R1, [R0, #0]
MOV	R0, R3
LDR	R0, [R0, #0]
ADDS	R0, R0, #4
LDRB	R0, [R0, #0]
MOVW	R2, #lo_addr(_gelen_akim+0)
MOVT	R2, #hi_addr(_gelen_akim+0)
STRH	R0, [R2, #0]
MOV	R0, R3
LDR	R0, [R0, #0]
ADDS	R0, R0, #6
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_deger+0)
MOVT	R0, #hi_addr(_deger+0)
STRB	R1, [R0, #0]
MOV	R0, R2
LDRH	R0, [R0, #0]
CMP	R0, #230
IT	CS
BCS	L__DSF52
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
CMP	R0, #20
IT	LS
BLS	L__DSF51
IT	AL
BAL	L_DSF24
L__DSF52:
L__DSF51:
MOVS	R1, #0
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
STRH	R1, [R0, #0]
L_DSF24:
;Node_1.c,120 :: 		gelen_adc_float1=(gelen_adc*4.43)/255; gelen_akim=gelen_akim*4;
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
LDRH	R0, [R0, #0]
BL	__UnsignedIntegralToFloat+0
MOVW	R2, #49807
MOVT	R2, #16525
BL	__Mul_FP+0
MOVW	R2, #0
MOVT	R2, #17279
BL	__Div_FP+0
MOVW	R1, #lo_addr(_gelen_adc_float1+0)
MOVT	R1, #hi_addr(_gelen_adc_float1+0)
STR	R0, [R1, #0]
MOVW	R1, #lo_addr(_gelen_akim+0)
MOVT	R1, #hi_addr(_gelen_akim+0)
LDRH	R0, [R1, #0]
LSLS	R0, R0, #2
STRH	R0, [R1, #0]
;Node_1.c,121 :: 		UART3_Write_Text("1.Batarya Voltaj�=");
MOVW	R0, #lo_addr(?lstr12_Node_1+0)
MOVT	R0, #hi_addr(?lstr12_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,122 :: 		Floattostr(gelen_adc_float1,txt1);
MOVW	R0, #lo_addr(_gelen_adc_float1+0)
MOVT	R0, #hi_addr(_gelen_adc_float1+0)
LDR	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_FloatToStr+0
;Node_1.c,123 :: 		UART3_Write_Text(txt1);
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
;Node_1.c,125 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,126 :: 		UART3_Write_Text("1.Batarya Ak�m�=");
MOVW	R0, #lo_addr(?lstr13_Node_1+0)
MOVT	R0, #hi_addr(?lstr13_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,127 :: 		Inttostr(gelen_akim,txt1);
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_IntToStr+0
;Node_1.c,128 :: 		UART3_Write_Text(txt1);   UART3_Write_Text(" mA");
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
MOVW	R0, #lo_addr(?lstr14_Node_1+0)
MOVT	R0, #hi_addr(?lstr14_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,129 :: 		UART3_Write(10);}
MOVS	R0, #10
BL	_UART3_Write+0
IT	AL
BAL	L_DSF25
L_DSF21:
;Node_1.c,131 :: 		else if(vassaf[1]=='2')
MOVW	R0, #lo_addr(_vassaf+0)
MOVT	R0, #hi_addr(_vassaf+0)
LDR	R0, [R0, #0]
ADDS	R0, R0, #1
LDRB	R0, [R0, #0]
CMP	R0, #50
IT	NE
BNE	L_DSF26
;Node_1.c,132 :: 		{gelen_adc=vassaf[3];gelen_akim=vassaf[4];if((gelen_akim>=230)||(gelen_akim<=20))gelen_akim=0;
MOVW	R2, #lo_addr(_vassaf+0)
MOVT	R2, #hi_addr(_vassaf+0)
LDR	R0, [R2, #0]
ADDS	R0, R0, #3
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
STRH	R1, [R0, #0]
MOV	R0, R2
LDR	R0, [R0, #0]
ADDS	R0, R0, #4
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
STRH	R1, [R0, #0]
LDRH	R0, [R0, #0]
CMP	R0, #230
IT	CS
BCS	L__DSF54
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
CMP	R0, #20
IT	LS
BLS	L__DSF53
IT	AL
BAL	L_DSF29
L__DSF54:
L__DSF53:
MOVS	R1, #0
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
STRH	R1, [R0, #0]
L_DSF29:
;Node_1.c,133 :: 		gelen_adc_float2=(gelen_adc*4.45)/255;  gelen_akim=gelen_akim*4;
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
LDRH	R0, [R0, #0]
BL	__UnsignedIntegralToFloat+0
MOVW	R2, #26214
MOVT	R2, #16526
BL	__Mul_FP+0
MOVW	R2, #0
MOVT	R2, #17279
BL	__Div_FP+0
MOVW	R1, #lo_addr(_gelen_adc_float2+0)
MOVT	R1, #hi_addr(_gelen_adc_float2+0)
STR	R0, [R1, #0]
MOVW	R1, #lo_addr(_gelen_akim+0)
MOVT	R1, #hi_addr(_gelen_akim+0)
LDRH	R0, [R1, #0]
LSLS	R0, R0, #2
STRH	R0, [R1, #0]
;Node_1.c,134 :: 		UART3_Write_Text("2.Batarya Voltaj�=");
MOVW	R0, #lo_addr(?lstr15_Node_1+0)
MOVT	R0, #hi_addr(?lstr15_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,135 :: 		Floattostr(gelen_adc_float2,txt1);
MOVW	R0, #lo_addr(_gelen_adc_float2+0)
MOVT	R0, #hi_addr(_gelen_adc_float2+0)
LDR	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_FloatToStr+0
;Node_1.c,137 :: 		UART3_Write_Text(txt1);
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
;Node_1.c,138 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,140 :: 		UART3_Write_Text("2.Batarya Ak�m�=");
MOVW	R0, #lo_addr(?lstr16_Node_1+0)
MOVT	R0, #hi_addr(?lstr16_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,141 :: 		Inttostr(gelen_akim,txt1);
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_IntToStr+0
;Node_1.c,142 :: 		UART3_Write_Text(txt1);   UART3_Write_Text(" mA");
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
MOVW	R0, #lo_addr(?lstr17_Node_1+0)
MOVT	R0, #hi_addr(?lstr17_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,143 :: 		UART3_Write(10);}
MOVS	R0, #10
BL	_UART3_Write+0
IT	AL
BAL	L_DSF30
L_DSF26:
;Node_1.c,145 :: 		else if(vassaf[1]=='3')
MOVW	R0, #lo_addr(_vassaf+0)
MOVT	R0, #hi_addr(_vassaf+0)
LDR	R0, [R0, #0]
ADDS	R0, R0, #1
LDRB	R0, [R0, #0]
CMP	R0, #51
IT	NE
BNE	L_DSF31
;Node_1.c,146 :: 		{gelen_adc=vassaf[3]; gelen_akim=vassaf[4]; if((gelen_akim>=230)||(gelen_akim<=20))gelen_akim=0;
MOVW	R2, #lo_addr(_vassaf+0)
MOVT	R2, #hi_addr(_vassaf+0)
LDR	R0, [R2, #0]
ADDS	R0, R0, #3
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
STRH	R1, [R0, #0]
MOV	R0, R2
LDR	R0, [R0, #0]
ADDS	R0, R0, #4
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
STRH	R1, [R0, #0]
LDRH	R0, [R0, #0]
CMP	R0, #230
IT	CS
BCS	L__DSF56
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
CMP	R0, #20
IT	LS
BLS	L__DSF55
IT	AL
BAL	L_DSF34
L__DSF56:
L__DSF55:
MOVS	R1, #0
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
STRH	R1, [R0, #0]
L_DSF34:
;Node_1.c,147 :: 		gelen_adc_float3=(gelen_adc*4.40)/255;    gelen_akim=gelen_akim*4;
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
LDRH	R0, [R0, #0]
BL	__UnsignedIntegralToFloat+0
MOVW	R2, #52429
MOVT	R2, #16524
BL	__Mul_FP+0
MOVW	R2, #0
MOVT	R2, #17279
BL	__Div_FP+0
MOVW	R1, #lo_addr(_gelen_adc_float3+0)
MOVT	R1, #hi_addr(_gelen_adc_float3+0)
STR	R0, [R1, #0]
MOVW	R1, #lo_addr(_gelen_akim+0)
MOVT	R1, #hi_addr(_gelen_akim+0)
LDRH	R0, [R1, #0]
LSLS	R0, R0, #2
STRH	R0, [R1, #0]
;Node_1.c,148 :: 		UART3_Write_Text("3.Batarya Voltaj�=");
MOVW	R0, #lo_addr(?lstr18_Node_1+0)
MOVT	R0, #hi_addr(?lstr18_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,149 :: 		Floattostr(gelen_adc_float3,txt1);
MOVW	R0, #lo_addr(_gelen_adc_float3+0)
MOVT	R0, #hi_addr(_gelen_adc_float3+0)
LDR	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_FloatToStr+0
;Node_1.c,150 :: 		UART3_Write_Text(txt1);
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
;Node_1.c,152 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,153 :: 		UART3_Write_Text("3.Batarya Ak�m�=");
MOVW	R0, #lo_addr(?lstr19_Node_1+0)
MOVT	R0, #hi_addr(?lstr19_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,154 :: 		Inttostr(gelen_akim,txt1);
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_IntToStr+0
;Node_1.c,155 :: 		UART3_Write_Text(txt1);      UART3_Write_Text(" mA");
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
MOVW	R0, #lo_addr(?lstr20_Node_1+0)
MOVT	R0, #hi_addr(?lstr20_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,156 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,164 :: 		}
IT	AL
BAL	L_DSF35
L_DSF31:
;Node_1.c,166 :: 		else if(vassaf[1]=='4')
MOVW	R0, #lo_addr(_vassaf+0)
MOVT	R0, #hi_addr(_vassaf+0)
LDR	R0, [R0, #0]
ADDS	R0, R0, #1
LDRB	R0, [R0, #0]
CMP	R0, #52
IT	NE
BNE	L_DSF36
;Node_1.c,167 :: 		{gelen_adc=vassaf[3]; gelen_akim=vassaf[4];   if((gelen_akim>=230)||(gelen_akim<=20))gelen_akim=0;
MOVW	R2, #lo_addr(_vassaf+0)
MOVT	R2, #hi_addr(_vassaf+0)
LDR	R0, [R2, #0]
ADDS	R0, R0, #3
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
STRH	R1, [R0, #0]
MOV	R0, R2
LDR	R0, [R0, #0]
ADDS	R0, R0, #4
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
STRH	R1, [R0, #0]
LDRH	R0, [R0, #0]
CMP	R0, #230
IT	CS
BCS	L__DSF58
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
CMP	R0, #20
IT	LS
BLS	L__DSF57
IT	AL
BAL	L_DSF39
L__DSF58:
L__DSF57:
MOVS	R1, #0
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
STRH	R1, [R0, #0]
L_DSF39:
;Node_1.c,168 :: 		gelen_adc_float4=(gelen_adc*4.40)/255;   gelen_akim=gelen_akim*4;
MOVW	R0, #lo_addr(_gelen_adc+0)
MOVT	R0, #hi_addr(_gelen_adc+0)
LDRH	R0, [R0, #0]
BL	__UnsignedIntegralToFloat+0
MOVW	R2, #52429
MOVT	R2, #16524
BL	__Mul_FP+0
MOVW	R2, #0
MOVT	R2, #17279
BL	__Div_FP+0
MOVW	R1, #lo_addr(_gelen_adc_float4+0)
MOVT	R1, #hi_addr(_gelen_adc_float4+0)
STR	R0, [R1, #0]
MOVW	R1, #lo_addr(_gelen_akim+0)
MOVT	R1, #hi_addr(_gelen_akim+0)
LDRH	R0, [R1, #0]
LSLS	R0, R0, #2
STRH	R0, [R1, #0]
;Node_1.c,169 :: 		UART3_Write_Text("4.Batarya Voltaj�=");
MOVW	R0, #lo_addr(?lstr21_Node_1+0)
MOVT	R0, #hi_addr(?lstr21_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,170 :: 		Floattostr(gelen_adc_float4,txt1);
MOVW	R0, #lo_addr(_gelen_adc_float4+0)
MOVT	R0, #hi_addr(_gelen_adc_float4+0)
LDR	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_FloatToStr+0
;Node_1.c,171 :: 		UART3_Write_Text(txt1);
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
;Node_1.c,172 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,173 :: 		UART3_Write_Text("4.Batarya Ak�m�=");
MOVW	R0, #lo_addr(?lstr22_Node_1+0)
MOVT	R0, #hi_addr(?lstr22_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,174 :: 		Inttostr(gelen_akim,txt1);
MOVW	R0, #lo_addr(_gelen_akim+0)
MOVT	R0, #hi_addr(_gelen_akim+0)
LDRH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_IntToStr+0
;Node_1.c,175 :: 		UART3_Write_Text(txt1);      UART3_Write_Text(" mA");
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
MOVW	R0, #lo_addr(?lstr23_Node_1+0)
MOVT	R0, #hi_addr(?lstr23_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,176 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,179 :: 		max_detect();  //H�crelerden voltaj� en y�ksek olan belirlendi.
BL	_max_detect+0
;Node_1.c,180 :: 		min_detect();  //H�crelerden voltaj� en d�s�k olan  belirlendi.
BL	_min_detect+0
;Node_1.c,181 :: 		diff=maximum-minimum;
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
LDR	R2, [R0, #0]
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
LDR	R0, [R0, #0]
BL	__Sub_FP+0
MOVW	R1, #lo_addr(_diff+0)
MOVT	R1, #hi_addr(_diff+0)
STR	R0, [R1, #0]
;Node_1.c,185 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,186 :: 		UART3_Write_Text("Maximum Voltage=");
MOVW	R0, #lo_addr(?lstr24_Node_1+0)
MOVT	R0, #hi_addr(?lstr24_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,187 :: 		Floattostr(maximum,txt1);
MOVW	R0, #lo_addr(_maximum+0)
MOVT	R0, #hi_addr(_maximum+0)
LDR	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_FloatToStr+0
;Node_1.c,188 :: 		UART3_Write_Text(txt1);
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
;Node_1.c,189 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,191 :: 		UART3_Write_Text("Minimum Voltage=");
MOVW	R0, #lo_addr(?lstr25_Node_1+0)
MOVT	R0, #hi_addr(?lstr25_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,192 :: 		Floattostr(minimum,txt1);
MOVW	R0, #lo_addr(_minimum+0)
MOVT	R0, #hi_addr(_minimum+0)
LDR	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_FloatToStr+0
;Node_1.c,193 :: 		UART3_Write_Text(txt1);
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
;Node_1.c,194 :: 		UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,195 :: 		UART3_Write_Text("Voltage Difference=");
MOVW	R0, #lo_addr(?lstr26_Node_1+0)
MOVT	R0, #hi_addr(?lstr26_Node_1+0)
BL	_UART3_Write_Text+0
;Node_1.c,196 :: 		Floattostr(diff,txt1);
MOVW	R0, #lo_addr(_diff+0)
MOVT	R0, #hi_addr(_diff+0)
LDR	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt1+0)
MOVT	R1, #hi_addr(_txt1+0)
BL	_FloatToStr+0
;Node_1.c,197 :: 		UART3_Write_Text(txt1);
MOVW	R0, #lo_addr(_txt1+0)
MOVT	R0, #hi_addr(_txt1+0)
BL	_UART3_Write_Text+0
;Node_1.c,198 :: 		UART3_Write(10);UART3_Write(10);
MOVS	R0, #10
BL	_UART3_Write+0
MOVS	R0, #10
BL	_UART3_Write+0
;Node_1.c,200 :: 		if(balance_flag)
MOVW	R0, #lo_addr(_balance_flag+0)
MOVT	R0, #hi_addr(_balance_flag+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_DSF40
;Node_1.c,201 :: 		UART3_Write_Text("Balancing...");
MOVW	R0, #lo_addr(?lstr27_Node_1+0)
MOVT	R0, #hi_addr(?lstr27_Node_1+0)
BL	_UART3_Write_Text+0
L_DSF40:
;Node_1.c,204 :: 		}
L_DSF36:
L_DSF35:
L_DSF30:
L_DSF25:
;Node_1.c,216 :: 		RXNE_USART1_SR_bit= 0  ;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(RXNE_USART1_SR_bit+0)
MOVT	R0, #hi_addr(RXNE_USART1_SR_bit+0)
STR	R1, [R0, #0]
;Node_1.c,217 :: 		}
L_DSF20:
;Node_1.c,218 :: 		}
L_end_DSF:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _DSF
_main:
;Node_1.c,225 :: 		void main()
;Node_1.c,227 :: 		UART3_Init_Advanced(9600, _UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART3_PB10_11);
MOVW	R0, #lo_addr(__GPIO_MODULE_USART3_PB10_11+0)
MOVT	R0, #hi_addr(__GPIO_MODULE_USART3_PB10_11+0)
PUSH	(R0)
MOVW	R3, #0
MOVW	R2, #0
MOVW	R1, #0
MOVW	R0, #9600
BL	_UART3_Init_Advanced+0
ADD	SP, SP, #4
;Node_1.c,229 :: 		UART1_Init_Advanced(9600,_UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART1_PB67);
MOVW	R0, #lo_addr(__GPIO_MODULE_USART1_PB67+0)
MOVT	R0, #hi_addr(__GPIO_MODULE_USART1_PB67+0)
PUSH	(R0)
MOVW	R3, #0
MOVW	R2, #0
MOVW	R1, #0
MOVW	R0, #9600
BL	_UART1_Init_Advanced+0
ADD	SP, SP, #4
;Node_1.c,233 :: 		GPIO_Digital_Output(&GPIOC_ODR, _GPIO_PINMASK_13);
MOVW	R1, #8192
MOVW	R0, #lo_addr(GPIOC_ODR+0)
MOVT	R0, #hi_addr(GPIOC_ODR+0)
BL	_GPIO_Digital_Output+0
;Node_1.c,236 :: 		EnableInterrupts();                                             // enable interrupts.
BL	_EnableInterrupts+0
;Node_1.c,238 :: 		RXNEIE_USART1_CR1_bit = 1;                                    // RX interrupt aktif edild.
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(RXNEIE_USART1_CR1_bit+0)
MOVT	R0, #hi_addr(RXNEIE_USART1_CR1_bit+0)
STR	R1, [R0, #0]
;Node_1.c,240 :: 		NVIC_IntEnable(IVT_INT_USART1);                            // USART1 interrupt aktif edildi.
MOVW	R0, #53
BL	_NVIC_IntEnable+0
;Node_1.c,243 :: 		x[0]='1';
MOVS	R1, #49
MOVW	R0, #lo_addr(_x+0)
MOVT	R0, #hi_addr(_x+0)
STRB	R1, [R0, #0]
;Node_1.c,244 :: 		x[1]='1';
MOVS	R1, #49
MOVW	R0, #lo_addr(_x+1)
MOVT	R0, #hi_addr(_x+1)
STRB	R1, [R0, #0]
;Node_1.c,245 :: 		x[2]='2';
MOVS	R1, #50
MOVW	R0, #lo_addr(_x+2)
MOVT	R0, #hi_addr(_x+2)
STRB	R1, [R0, #0]
;Node_1.c,246 :: 		x[3]='2';
MOVS	R1, #50
MOVW	R0, #lo_addr(_x+3)
MOVT	R0, #hi_addr(_x+3)
STRB	R1, [R0, #0]
;Node_1.c,248 :: 		gonder();
BL	_gonder+0
;Node_1.c,249 :: 		delay_ms(100);
MOVW	R7, #20351
MOVT	R7, #18
NOP
NOP
L_main41:
SUBS	R7, R7, #1
BNE	L_main41
NOP
NOP
NOP
;Node_1.c,250 :: 		while(1)
L_main43:
;Node_1.c,254 :: 		gonder();
BL	_gonder+0
;Node_1.c,257 :: 		if(diff>=0.05)   // Maximum ve minimum h�creler arasinda 50mV fark varsa balans baslatiliyor.
MOVW	R0, #lo_addr(_diff+0)
MOVT	R0, #hi_addr(_diff+0)
LDR	R2, [R0, #0]
MOVW	R0, #52429
MOVT	R0, #15692
BL	__Compare_FP+0
MOVW	R0, #0
BGT	L__main70
MOVS	R0, #1
L__main70:
CMP	R0, #0
IT	EQ
BEQ	L_main45
;Node_1.c,258 :: 		{balance_flag=1;}
MOVS	R1, #1
MOVW	R0, #lo_addr(_balance_flag+0)
MOVT	R0, #hi_addr(_balance_flag+0)
STRB	R1, [R0, #0]
IT	AL
BAL	L_main46
L_main45:
;Node_1.c,259 :: 		else {balance_flag=0;}
MOVS	R1, #0
MOVW	R0, #lo_addr(_balance_flag+0)
MOVT	R0, #hi_addr(_balance_flag+0)
STRB	R1, [R0, #0]
L_main46:
;Node_1.c,270 :: 		}}
IT	AL
BAL	L_main43
L_end_main:
L__main_end_loop:
B	L__main_end_loop
; end of _main
