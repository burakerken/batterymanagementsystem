
_read_and_filtre:

;Node2.c,25 :: 		int read_and_filtre(short int ornek, short int pin, short int filtre)
;Node2.c,27 :: 		for (i=0;i<ornek;i++){ dd[i]=ADC_Read(pin);Delay_ms(1); }
	CLRF        _i+0 
	CLRF        _i+1 
L_read_and_filtre0:
	MOVLW       0
	BTFSC       FARG_read_and_filtre_ornek+0, 7 
	MOVLW       255
	SUBWF       _i+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__read_and_filtre113
	MOVF        FARG_read_and_filtre_ornek+0, 0 
	SUBWF       _i+0, 0 
L__read_and_filtre113:
	BTFSC       STATUS+0, 0 
	GOTO        L_read_and_filtre1
	MOVF        _i+0, 0 
	MOVWF       R0 
	MOVF        _i+1, 0 
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       _dd+0
	ADDWF       R0, 0 
	MOVWF       FLOC__read_and_filtre+0 
	MOVLW       hi_addr(_dd+0)
	ADDWFC      R1, 0 
	MOVWF       FLOC__read_and_filtre+1 
	MOVF        FARG_read_and_filtre_pin+0, 0 
	MOVWF       FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVFF       FLOC__read_and_filtre+0, FSR1
	MOVFF       FLOC__read_and_filtre+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
	MOVF        R1, 0 
	MOVWF       POSTINC1+0 
	MOVLW       4
	MOVWF       R12, 0
	MOVLW       61
	MOVWF       R13, 0
L_read_and_filtre3:
	DECFSZ      R13, 1, 1
	BRA         L_read_and_filtre3
	DECFSZ      R12, 1, 1
	BRA         L_read_and_filtre3
	NOP
	NOP
	INFSNZ      _i+0, 1 
	INCF        _i+1, 1 
	GOTO        L_read_and_filtre0
L_read_and_filtre1:
;Node2.c,29 :: 		i=0;
	CLRF        _i+0 
	CLRF        _i+1 
;Node2.c,31 :: 		{ for(k=0;k<ornek;k++)                                               //diziyi b�y�kten k���ge s�rala
	CLRF        _k+0 
L_read_and_filtre4:
	MOVLW       128
	MOVWF       R0 
	MOVLW       128
	BTFSC       FARG_read_and_filtre_ornek+0, 7 
	MOVLW       127
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__read_and_filtre114
	MOVF        FARG_read_and_filtre_ornek+0, 0 
	SUBWF       _k+0, 0 
L__read_and_filtre114:
	BTFSC       STATUS+0, 0 
	GOTO        L_read_and_filtre5
;Node2.c,32 :: 		{  for(j=0;j<ornek;j++)                                       //k=0 ve j=1 i�in olan durumu inceleyelim.
	CLRF        _j+0 
	CLRF        _j+1 
L_read_and_filtre7:
	MOVLW       0
	BTFSC       FARG_read_and_filtre_ornek+0, 7 
	MOVLW       255
	SUBWF       _j+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__read_and_filtre115
	MOVF        FARG_read_and_filtre_ornek+0, 0 
	SUBWF       _j+0, 0 
L__read_and_filtre115:
	BTFSC       STATUS+0, 0 
	GOTO        L_read_and_filtre8
;Node2.c,33 :: 		{ if(dd[k]>dd[j])                                            //dd0 de�eri dd1'den b�y�k ise dd0 ile dd1 de�erlerini yer de�i�tiriyor.
	MOVF        _k+0, 0 
	MOVWF       R0 
	MOVLW       0
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       _dd+0
	ADDWF       R0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_dd+0)
	ADDWFC      R1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       R3 
	MOVF        POSTINC0+0, 0 
	MOVWF       R4 
	MOVF        _j+0, 0 
	MOVWF       R0 
	MOVF        _j+1, 0 
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       _dd+0
	ADDWF       R0, 0 
	MOVWF       FSR2 
	MOVLW       hi_addr(_dd+0)
	ADDWFC      R1, 0 
	MOVWF       FSR2H 
	MOVF        POSTINC2+0, 0 
	MOVWF       R1 
	MOVF        POSTINC2+0, 0 
	MOVWF       R2 
	MOVF        R4, 0 
	SUBWF       R2, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__read_and_filtre116
	MOVF        R3, 0 
	SUBWF       R1, 0 
L__read_and_filtre116:
	BTFSC       STATUS+0, 0 
	GOTO        L_read_and_filtre10
;Node2.c,34 :: 		{ g=dd[k];                                                     //b�y�k olan de�eri g ye kopyal�yor.
	MOVF        _k+0, 0 
	MOVWF       R0 
	MOVLW       0
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       _dd+0
	ADDWF       R0, 0 
	MOVWF       R3 
	MOVLW       hi_addr(_dd+0)
	ADDWFC      R1, 0 
	MOVWF       R4 
	MOVFF       R3, FSR0
	MOVFF       R4, FSR0H
	MOVF        POSTINC0+0, 0 
	MOVWF       _g+0 
	MOVF        POSTINC0+0, 0 
	MOVWF       _g+1 
;Node2.c,35 :: 		dd[k]=dd[j];                                                  //dd1'deki k���k olan de�eri dd0'a y�kl�yor.
	MOVF        _j+0, 0 
	MOVWF       R0 
	MOVF        _j+1, 0 
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       _dd+0
	ADDWF       R0, 0 
	MOVWF       FSR0 
	MOVLW       hi_addr(_dd+0)
	ADDWFC      R1, 0 
	MOVWF       FSR0H 
	MOVFF       R3, FSR1
	MOVFF       R4, FSR1H
	MOVF        POSTINC0+0, 0 
	MOVWF       POSTINC1+0 
	MOVF        POSTINC0+0, 0 
	MOVWF       POSTINC1+0 
;Node2.c,36 :: 		dd[j]=g;                                                      //dd1'ede dd0 daki b�y�k de�eri y�kl�yor.
	MOVF        _j+0, 0 
	MOVWF       R0 
	MOVF        _j+1, 0 
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       _dd+0
	ADDWF       R0, 0 
	MOVWF       FSR1 
	MOVLW       hi_addr(_dd+0)
	ADDWFC      R1, 0 
	MOVWF       FSR1H 
	MOVF        _g+0, 0 
	MOVWF       POSTINC1+0 
	MOVF        _g+1, 0 
	MOVWF       POSTINC1+0 
;Node2.c,37 :: 		} } }
L_read_and_filtre10:
;Node2.c,32 :: 		{  for(j=0;j<ornek;j++)                                       //k=0 ve j=1 i�in olan durumu inceleyelim.
	INFSNZ      _j+0, 1 
	INCF        _j+1, 1 
;Node2.c,37 :: 		} } }
	GOTO        L_read_and_filtre7
L_read_and_filtre8:
;Node2.c,31 :: 		{ for(k=0;k<ornek;k++)                                               //diziyi b�y�kten k���ge s�rala
	INCF        _k+0, 1 
;Node2.c,37 :: 		} } }
	GOTO        L_read_and_filtre4
L_read_and_filtre5:
;Node2.c,39 :: 		for(k=filtre;k<(ornek-filtre);k++)                            // s�ralanan dizinin en b�y�k ve k���k de�erlerini sil kalan de�erleri topla
	MOVF        FARG_read_and_filtre_filtre+0, 0 
	MOVWF       _k+0 
L_read_and_filtre11:
	MOVF        FARG_read_and_filtre_filtre+0, 0 
	SUBWF       FARG_read_and_filtre_ornek+0, 0 
	MOVWF       R1 
	MOVLW       0
	BTFSC       FARG_read_and_filtre_ornek+0, 7 
	MOVLW       255
	MOVWF       R2 
	MOVLW       0
	BTFSC       FARG_read_and_filtre_filtre+0, 7 
	MOVLW       255
	SUBWFB      R2, 1 
	MOVLW       128
	MOVWF       R0 
	MOVLW       128
	XORWF       R2, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__read_and_filtre117
	MOVF        R1, 0 
	SUBWF       _k+0, 0 
L__read_and_filtre117:
	BTFSC       STATUS+0, 0 
	GOTO        L_read_and_filtre12
;Node2.c,40 :: 		{ toplam +=dd[k]; }
	MOVF        _k+0, 0 
	MOVWF       R0 
	MOVLW       0
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       _dd+0
	ADDWF       R0, 0 
	MOVWF       FSR2 
	MOVLW       hi_addr(_dd+0)
	ADDWFC      R1, 0 
	MOVWF       FSR2H 
	MOVF        POSTINC2+0, 0 
	ADDWF       _toplam+0, 1 
	MOVF        POSTINC2+0, 0 
	ADDWFC      _toplam+1, 1 
;Node2.c,39 :: 		for(k=filtre;k<(ornek-filtre);k++)                            // s�ralanan dizinin en b�y�k ve k���k de�erlerini sil kalan de�erleri topla
	INCF        _k+0, 1 
;Node2.c,40 :: 		{ toplam +=dd[k]; }
	GOTO        L_read_and_filtre11
L_read_and_filtre12:
;Node2.c,42 :: 		adcort=toplam/(ornek-2*filtre);         // kalan de�erlerin ortalamas�n� al
	MOVF        FARG_read_and_filtre_filtre+0, 0 
	MOVWF       R0 
	MOVLW       0
	BTFSC       FARG_read_and_filtre_filtre+0, 7 
	MOVLW       255
	MOVWF       R1 
	RLCF        R0, 1 
	RLCF        R1, 1 
	BCF         R0, 0 
	MOVF        R0, 0 
	SUBWF       FARG_read_and_filtre_ornek+0, 0 
	MOVWF       R4 
	MOVF        R1, 0 
	MOVWF       R5 
	MOVLW       0
	BTFSC       FARG_read_and_filtre_ornek+0, 7 
	MOVLW       255
	SUBFWB      R5, 1 
	MOVF        _toplam+0, 0 
	MOVWF       R0 
	MOVF        _toplam+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_U+0, 0
	MOVF        R0, 0 
	MOVWF       _adcort+0 
	MOVF        R1, 0 
	MOVWF       _adcort+1 
;Node2.c,43 :: 		toplam=0; return adcort;                                 // toplam de�i�kenini sonraki filtre i�lemi i�in s�rala
	CLRF        _toplam+0 
	CLRF        _toplam+1 
;Node2.c,44 :: 		} }
L_end_read_and_filtre:
	RETURN      0
; end of _read_and_filtre

_interrupt:

;Node2.c,47 :: 		void interrupt()
;Node2.c,49 :: 		if(PIR1.RCIF)
	BTFSS       PIR1+0, 5 
	GOTO        L_interrupt14
;Node2.c,51 :: 		PIR1.RCIF=0;
	BCF         PIR1+0, 5 
;Node2.c,52 :: 		PORTC.F0=1;
	BSF         PORTC+0, 0 
;Node2.c,53 :: 		UART1_Read_Text(output,"OK", 30);    // reads text until 'OK' is found
	MOVF        _output+0, 0 
	MOVWF       FARG_UART1_Read_Text_Output+0 
	MOVF        _output+1, 0 
	MOVWF       FARG_UART1_Read_Text_Output+1 
	MOVLW       ?lstr2_Node2+0
	MOVWF       FARG_UART1_Read_Text_Delimiter+0 
	MOVLW       hi_addr(?lstr2_Node2+0)
	MOVWF       FARG_UART1_Read_Text_Delimiter+1 
	MOVLW       30
	MOVWF       FARG_UART1_Read_Text_Attempts+0 
	CALL        _UART1_Read_Text+0, 0
;Node2.c,55 :: 		if(output[1]=='2'){output[3]=voltage;output[4]=akim_sonuc_char;voltage_istenen=output[5];istenen=output[6]; }
	MOVLW       1
	ADDWF       _output+0, 0 
	MOVWF       FSR0 
	MOVLW       0
	ADDWFC      _output+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	XORLW       50
	BTFSS       STATUS+0, 2 
	GOTO        L_interrupt15
	MOVLW       3
	ADDWF       _output+0, 0 
	MOVWF       FSR1 
	MOVLW       0
	ADDWFC      _output+1, 0 
	MOVWF       FSR1H 
	MOVF        _voltage+0, 0 
	MOVWF       POSTINC1+0 
	MOVLW       4
	ADDWF       _output+0, 0 
	MOVWF       FSR1 
	MOVLW       0
	ADDWFC      _output+1, 0 
	MOVWF       FSR1H 
	MOVF        _akim_sonuc_char+0, 0 
	MOVWF       POSTINC1+0 
	MOVLW       5
	ADDWF       _output+0, 0 
	MOVWF       FSR0 
	MOVLW       0
	ADDWFC      _output+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       _voltage_istenen+0 
	MOVLW       6
	ADDWF       _output+0, 0 
	MOVWF       FSR0 
	MOVLW       0
	ADDWFC      _output+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       _istenen+0 
L_interrupt15:
;Node2.c,58 :: 		UART1_Write_Text(output);
	MOVF        _output+0, 0 
	MOVWF       FARG_UART1_Write_Text_uart_text+0 
	MOVF        _output+1, 0 
	MOVWF       FARG_UART1_Write_Text_uart_text+1 
	CALL        _UART1_Write_Text+0, 0
;Node2.c,60 :: 		UART1_Write(79);
	MOVLW       79
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;Node2.c,62 :: 		UART1_Write(75);
	MOVLW       75
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;Node2.c,63 :: 		PORTC.F0=0;
	BCF         PORTC+0, 0 
;Node2.c,64 :: 		PIR1.RCIF=0;
	BCF         PIR1+0, 5 
;Node2.c,66 :: 		}
L_interrupt14:
;Node2.c,67 :: 		}
L_end_interrupt:
L__interrupt119:
	RETFIE      1
; end of _interrupt

_main:

;Node2.c,75 :: 		void main()
;Node2.c,78 :: 		UART1_Init(9600);    // UART 9600 Baudrate ile baslat�ld�.
	BSF         BAUDCON+0, 3, 0
	MOVLW       1
	MOVWF       SPBRGH+0 
	MOVLW       3
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;Node2.c,79 :: 		INTCON.GIE = 1;      // GLOBAL INTERRUPT ENABLE
	BSF         INTCON+0, 7 
;Node2.c,80 :: 		PIE1.RCIE = 1;       // RECIEVE INTERRUPT ENABLE
	BSF         PIE1+0, 5 
;Node2.c,81 :: 		INTCON.PEIE = 1;     // peripheral intterrupt
	BSF         INTCON+0, 6 
;Node2.c,82 :: 		PIR1.RCIF=0;         // RX FLAG BIT
	BCF         PIR1+0, 5 
;Node2.c,83 :: 		RCSTA.CREN=1;
	BSF         RCSTA+0, 4 
;Node2.c,87 :: 		TRISC=0x10000000;
	MOVLW       0
	MOVWF       TRISC+0 
;Node2.c,88 :: 		PORTC=0x00;
	CLRF        PORTC+0 
;Node2.c,90 :: 		ADCON1|=0x07;
	MOVLW       7
	IORWF       ADCON1+0, 1 
;Node2.c,91 :: 		CMCON |=0x07;
	MOVLW       7
	IORWF       CMCON+0, 1 
;Node2.c,92 :: 		ADC_Init();
	CALL        _ADC_Init+0, 0
;Node2.c,95 :: 		PWM1_Init(5000);
	BSF         T2CON+0, 0, 0
	BCF         T2CON+0, 1, 0
	MOVLW       124
	MOVWF       PR2+0, 0
	CALL        _PWM1_Init+0, 0
;Node2.c,96 :: 		PWM1_Set_Duty(0);
	CLRF        FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
;Node2.c,97 :: 		PWM1_Start();
	CALL        _PWM1_Start+0, 0
;Node2.c,99 :: 		while(1)
L_main16:
;Node2.c,101 :: 		if(RCSTA.OERR==1); RCSTA.CREN=1;  PORTC.F0=0;
	BTFSS       RCSTA+0, 1 
	GOTO        L_main18
L_main18:
	BSF         RCSTA+0, 4 
	BCF         PORTC+0, 0 
;Node2.c,103 :: 		voltage=read_and_filtre(50,3,10);  // 3. kanaldan okundu
	MOVLW       50
	MOVWF       FARG_read_and_filtre_ornek+0 
	MOVLW       3
	MOVWF       FARG_read_and_filtre_pin+0 
	MOVLW       10
	MOVWF       FARG_read_and_filtre_filtre+0 
	CALL        _read_and_filtre+0, 0
	MOVF        R0, 0 
	MOVWF       _voltage+0 
	MOVF        R1, 0 
	MOVWF       _voltage+1 
;Node2.c,104 :: 		new1=voltage;
	MOVF        R0, 0 
	MOVWF       _new1+0 
	MOVF        R1, 0 
	MOVWF       _new1+1 
;Node2.c,105 :: 		voltage=voltage>>2;                // g�nderilen voltaj de�eri
	MOVF        R0, 0 
	MOVWF       _voltage+0 
	MOVF        R1, 0 
	MOVWF       _voltage+1 
	RRCF        _voltage+1, 1 
	RRCF        _voltage+0, 1 
	BCF         _voltage+1, 7 
	RRCF        _voltage+1, 1 
	RRCF        _voltage+0, 1 
	BCF         _voltage+1, 7 
;Node2.c,108 :: 		akim_voltage=read_and_filtre(50,9,10);      // 9. kanaldan okundu
	MOVLW       50
	MOVWF       FARG_read_and_filtre_ornek+0 
	MOVLW       9
	MOVWF       FARG_read_and_filtre_pin+0 
	MOVLW       10
	MOVWF       FARG_read_and_filtre_filtre+0 
	CALL        _read_and_filtre+0, 0
	MOVF        R0, 0 
	MOVWF       _akim_voltage+0 
	MOVF        R1, 0 
	MOVWF       _akim_voltage+1 
;Node2.c,109 :: 		new2=akim_voltage;
	MOVF        R0, 0 
	MOVWF       _new2+0 
	MOVF        R1, 0 
	MOVWF       _new2+1 
;Node2.c,110 :: 		akim_voltage=akim_voltage>>2;
	MOVF        R0, 0 
	MOVWF       _akim_voltage+0 
	MOVF        R1, 0 
	MOVWF       _akim_voltage+1 
	RRCF        _akim_voltage+1, 1 
	RRCF        _akim_voltage+0, 1 
	BCF         _akim_voltage+1, 7 
	RRCF        _akim_voltage+1, 1 
	RRCF        _akim_voltage+0, 1 
	BCF         _akim_voltage+1, 7 
;Node2.c,112 :: 		akim_sonuc_f=((new1-new2)*4.4)/4700;
	MOVF        R0, 0 
	SUBWF       _new1+0, 0 
	MOVWF       R0 
	MOVF        R1, 0 
	SUBWFB      _new1+1, 0 
	MOVWF       R1 
	CALL        _Word2Double+0, 0
	MOVLW       205
	MOVWF       R4 
	MOVLW       204
	MOVWF       R5 
	MOVLW       12
	MOVWF       R6 
	MOVLW       129
	MOVWF       R7 
	CALL        _Mul_32x32_FP+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       224
	MOVWF       R5 
	MOVLW       18
	MOVWF       R6 
	MOVLW       139
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _akim_sonuc_f+0 
	MOVF        R1, 0 
	MOVWF       _akim_sonuc_f+1 
	MOVF        R2, 0 
	MOVWF       _akim_sonuc_f+2 
	MOVF        R3, 0 
	MOVWF       _akim_sonuc_f+3 
;Node2.c,113 :: 		akim_sonuc_f=(akim_sonuc_f*1000)+70;      // d�zeltme yap�ld�. Filtrelerden kaynakl� �l��mler farkl�
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       122
	MOVWF       R6 
	MOVLW       136
	MOVWF       R7 
	CALL        _Mul_32x32_FP+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       12
	MOVWF       R6 
	MOVLW       133
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _akim_sonuc_f+0 
	MOVF        R1, 0 
	MOVWF       _akim_sonuc_f+1 
	MOVF        R2, 0 
	MOVWF       _akim_sonuc_f+2 
	MOVF        R3, 0 
	MOVWF       _akim_sonuc_f+3 
;Node2.c,114 :: 		akim_sonuc=akim_sonuc_f;   // integere �evrildi ,  mA �eklinde ��k�� �rnek:  67 mA       --- uart ile g�nderilebilir.
	CALL        _Double2Word+0, 0
	MOVF        R0, 0 
	MOVWF       _akim_sonuc+0 
	MOVF        R1, 0 
	MOVWF       _akim_sonuc+1 
;Node2.c,115 :: 		akim_sonuc_char=akim_sonuc>>2;// uart ile g�nderilecek akim degeri- kar�� tarafta 4 ile �arp�lacak.
	MOVF        R0, 0 
	MOVWF       R2 
	MOVF        R1, 0 
	MOVWF       R3 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	RRCF        R3, 1 
	RRCF        R2, 1 
	BCF         R3, 7 
	MOVF        R2, 0 
	MOVWF       _akim_sonuc_char+0 
;Node2.c,117 :: 		if(current_duty>=200)current_duty=200;
	MOVLW       0
	SUBWF       _current_duty+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main121
	MOVLW       200
	SUBWF       _current_duty+0, 0 
L__main121:
	BTFSS       STATUS+0, 0 
	GOTO        L_main19
	MOVLW       200
	MOVWF       _current_duty+0 
	MOVLW       0
	MOVWF       _current_duty+1 
L_main19:
;Node2.c,120 :: 		if((istenen=='1')&&(akim_sonuc>=150))
	MOVF        _istenen+0, 0 
	XORLW       49
	BTFSS       STATUS+0, 2 
	GOTO        L_main22
	MOVLW       0
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main122
	MOVLW       150
	SUBWF       _akim_sonuc+0, 0 
L__main122:
	BTFSS       STATUS+0, 0 
	GOTO        L_main22
L__main111:
;Node2.c,122 :: 		current_duty--;
	MOVLW       1
	SUBWF       _current_duty+0, 1 
	MOVLW       0
	SUBWFB      _current_duty+1, 1 
;Node2.c,123 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main23:
	DECFSZ      R13, 1, 1
	BRA         L_main23
	NOP
;Node2.c,124 :: 		}else if((istenen=='1')&&(akim_sonuc<=50)){
	GOTO        L_main24
L_main22:
	MOVF        _istenen+0, 0 
	XORLW       49
	BTFSS       STATUS+0, 2 
	GOTO        L_main27
	MOVLW       0
	MOVWF       R0 
	MOVF        _akim_sonuc+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main123
	MOVF        _akim_sonuc+0, 0 
	SUBLW       50
L__main123:
	BTFSS       STATUS+0, 0 
	GOTO        L_main27
L__main110:
;Node2.c,125 :: 		current_duty++;
	INFSNZ      _current_duty+0, 1 
	INCF        _current_duty+1, 1 
;Node2.c,126 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main28:
	DECFSZ      R13, 1, 1
	BRA         L_main28
	NOP
;Node2.c,127 :: 		}
	GOTO        L_main29
L_main27:
;Node2.c,128 :: 		else if((istenen=='1')&&(akim_sonuc>50)&&(akim_sonuc<150)){
	MOVF        _istenen+0, 0 
	XORLW       49
	BTFSS       STATUS+0, 2 
	GOTO        L_main32
	MOVLW       0
	MOVWF       R0 
	MOVF        _akim_sonuc+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main124
	MOVF        _akim_sonuc+0, 0 
	SUBLW       50
L__main124:
	BTFSC       STATUS+0, 0 
	GOTO        L_main32
	MOVLW       0
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main125
	MOVLW       150
	SUBWF       _akim_sonuc+0, 0 
L__main125:
	BTFSC       STATUS+0, 0 
	GOTO        L_main32
L__main109:
;Node2.c,130 :: 		PWM1_Set_Duty(current_duty);delay_ms(10);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       33
	MOVWF       R12, 0
	MOVLW       118
	MOVWF       R13, 0
L_main33:
	DECFSZ      R13, 1, 1
	BRA         L_main33
	DECFSZ      R12, 1, 1
	BRA         L_main33
	NOP
;Node2.c,131 :: 		}
	GOTO        L_main34
L_main32:
;Node2.c,136 :: 		else if((istenen=='2')&&(akim_sonuc>=300)){
	MOVF        _istenen+0, 0 
	XORLW       50
	BTFSS       STATUS+0, 2 
	GOTO        L_main37
	MOVLW       1
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main126
	MOVLW       44
	SUBWF       _akim_sonuc+0, 0 
L__main126:
	BTFSS       STATUS+0, 0 
	GOTO        L_main37
L__main108:
;Node2.c,138 :: 		current_duty--;
	MOVLW       1
	SUBWF       _current_duty+0, 1 
	MOVLW       0
	SUBWFB      _current_duty+1, 1 
;Node2.c,139 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main38:
	DECFSZ      R13, 1, 1
	BRA         L_main38
	NOP
;Node2.c,140 :: 		}else if((istenen=='2')&&(akim_sonuc<=150)){
	GOTO        L_main39
L_main37:
	MOVF        _istenen+0, 0 
	XORLW       50
	BTFSS       STATUS+0, 2 
	GOTO        L_main42
	MOVLW       0
	MOVWF       R0 
	MOVF        _akim_sonuc+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main127
	MOVF        _akim_sonuc+0, 0 
	SUBLW       150
L__main127:
	BTFSS       STATUS+0, 0 
	GOTO        L_main42
L__main107:
;Node2.c,141 :: 		current_duty++;
	INFSNZ      _current_duty+0, 1 
	INCF        _current_duty+1, 1 
;Node2.c,142 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main43:
	DECFSZ      R13, 1, 1
	BRA         L_main43
	NOP
;Node2.c,143 :: 		}
	GOTO        L_main44
L_main42:
;Node2.c,144 :: 		else if((istenen=='2')&&(akim_sonuc>150)&&(akim_sonuc<300)){
	MOVF        _istenen+0, 0 
	XORLW       50
	BTFSS       STATUS+0, 2 
	GOTO        L_main47
	MOVLW       0
	MOVWF       R0 
	MOVF        _akim_sonuc+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main128
	MOVF        _akim_sonuc+0, 0 
	SUBLW       150
L__main128:
	BTFSC       STATUS+0, 0 
	GOTO        L_main47
	MOVLW       1
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main129
	MOVLW       44
	SUBWF       _akim_sonuc+0, 0 
L__main129:
	BTFSC       STATUS+0, 0 
	GOTO        L_main47
L__main106:
;Node2.c,146 :: 		PWM1_Set_Duty(current_duty);delay_ms(10);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       33
	MOVWF       R12, 0
	MOVLW       118
	MOVWF       R13, 0
L_main48:
	DECFSZ      R13, 1, 1
	BRA         L_main48
	DECFSZ      R12, 1, 1
	BRA         L_main48
	NOP
;Node2.c,147 :: 		}
	GOTO        L_main49
L_main47:
;Node2.c,150 :: 		else if((istenen=='3')&&(akim_sonuc>=450)){
	MOVF        _istenen+0, 0 
	XORLW       51
	BTFSS       STATUS+0, 2 
	GOTO        L_main52
	MOVLW       1
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main130
	MOVLW       194
	SUBWF       _akim_sonuc+0, 0 
L__main130:
	BTFSS       STATUS+0, 0 
	GOTO        L_main52
L__main105:
;Node2.c,151 :: 		current_duty--;
	MOVLW       1
	SUBWF       _current_duty+0, 1 
	MOVLW       0
	SUBWFB      _current_duty+1, 1 
;Node2.c,152 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main53:
	DECFSZ      R13, 1, 1
	BRA         L_main53
	NOP
;Node2.c,153 :: 		}else if((istenen=='3')&&(akim_sonuc<=300)){
	GOTO        L_main54
L_main52:
	MOVF        _istenen+0, 0 
	XORLW       51
	BTFSS       STATUS+0, 2 
	GOTO        L_main57
	MOVF        _akim_sonuc+1, 0 
	SUBLW       1
	BTFSS       STATUS+0, 2 
	GOTO        L__main131
	MOVF        _akim_sonuc+0, 0 
	SUBLW       44
L__main131:
	BTFSS       STATUS+0, 0 
	GOTO        L_main57
L__main104:
;Node2.c,154 :: 		current_duty++;
	INFSNZ      _current_duty+0, 1 
	INCF        _current_duty+1, 1 
;Node2.c,155 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main58:
	DECFSZ      R13, 1, 1
	BRA         L_main58
	NOP
;Node2.c,156 :: 		}else if((istenen=='3')&&(akim_sonuc>300)&&(akim_sonuc<450)){
	GOTO        L_main59
L_main57:
	MOVF        _istenen+0, 0 
	XORLW       51
	BTFSS       STATUS+0, 2 
	GOTO        L_main62
	MOVF        _akim_sonuc+1, 0 
	SUBLW       1
	BTFSS       STATUS+0, 2 
	GOTO        L__main132
	MOVF        _akim_sonuc+0, 0 
	SUBLW       44
L__main132:
	BTFSC       STATUS+0, 0 
	GOTO        L_main62
	MOVLW       1
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main133
	MOVLW       194
	SUBWF       _akim_sonuc+0, 0 
L__main133:
	BTFSC       STATUS+0, 0 
	GOTO        L_main62
L__main103:
;Node2.c,157 :: 		PWM1_Set_Duty(current_duty);delay_ms(10);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       33
	MOVWF       R12, 0
	MOVLW       118
	MOVWF       R13, 0
L_main63:
	DECFSZ      R13, 1, 1
	BRA         L_main63
	DECFSZ      R12, 1, 1
	BRA         L_main63
	NOP
;Node2.c,158 :: 		}
	GOTO        L_main64
L_main62:
;Node2.c,161 :: 		else if((istenen=='4')&&(akim_sonuc>=600)){
	MOVF        _istenen+0, 0 
	XORLW       52
	BTFSS       STATUS+0, 2 
	GOTO        L_main67
	MOVLW       2
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main134
	MOVLW       88
	SUBWF       _akim_sonuc+0, 0 
L__main134:
	BTFSS       STATUS+0, 0 
	GOTO        L_main67
L__main102:
;Node2.c,162 :: 		current_duty--;
	MOVLW       1
	SUBWF       _current_duty+0, 1 
	MOVLW       0
	SUBWFB      _current_duty+1, 1 
;Node2.c,163 :: 		PWM1_Set_Duty(current_duty); delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main68:
	DECFSZ      R13, 1, 1
	BRA         L_main68
	NOP
;Node2.c,164 :: 		}else if((istenen=='4')&&(akim_sonuc<=450)){
	GOTO        L_main69
L_main67:
	MOVF        _istenen+0, 0 
	XORLW       52
	BTFSS       STATUS+0, 2 
	GOTO        L_main72
	MOVF        _akim_sonuc+1, 0 
	SUBLW       1
	BTFSS       STATUS+0, 2 
	GOTO        L__main135
	MOVF        _akim_sonuc+0, 0 
	SUBLW       194
L__main135:
	BTFSS       STATUS+0, 0 
	GOTO        L_main72
L__main101:
;Node2.c,165 :: 		current_duty++;
	INFSNZ      _current_duty+0, 1 
	INCF        _current_duty+1, 1 
;Node2.c,166 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main73:
	DECFSZ      R13, 1, 1
	BRA         L_main73
	NOP
;Node2.c,167 :: 		}else if((istenen=='4')&&(akim_sonuc>450)&&(akim_sonuc<600)){
	GOTO        L_main74
L_main72:
	MOVF        _istenen+0, 0 
	XORLW       52
	BTFSS       STATUS+0, 2 
	GOTO        L_main77
	MOVF        _akim_sonuc+1, 0 
	SUBLW       1
	BTFSS       STATUS+0, 2 
	GOTO        L__main136
	MOVF        _akim_sonuc+0, 0 
	SUBLW       194
L__main136:
	BTFSC       STATUS+0, 0 
	GOTO        L_main77
	MOVLW       2
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main137
	MOVLW       88
	SUBWF       _akim_sonuc+0, 0 
L__main137:
	BTFSC       STATUS+0, 0 
	GOTO        L_main77
L__main100:
;Node2.c,168 :: 		PWM1_Set_Duty(current_duty);delay_ms(10);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       33
	MOVWF       R12, 0
	MOVLW       118
	MOVWF       R13, 0
L_main78:
	DECFSZ      R13, 1, 1
	BRA         L_main78
	DECFSZ      R12, 1, 1
	BRA         L_main78
	NOP
;Node2.c,169 :: 		}
	GOTO        L_main79
L_main77:
;Node2.c,171 :: 		else if((istenen=='5')&&(akim_sonuc>=900)){
	MOVF        _istenen+0, 0 
	XORLW       53
	BTFSS       STATUS+0, 2 
	GOTO        L_main82
	MOVLW       3
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main138
	MOVLW       132
	SUBWF       _akim_sonuc+0, 0 
L__main138:
	BTFSS       STATUS+0, 0 
	GOTO        L_main82
L__main99:
;Node2.c,172 :: 		current_duty--;
	MOVLW       1
	SUBWF       _current_duty+0, 1 
	MOVLW       0
	SUBWFB      _current_duty+1, 1 
;Node2.c,173 :: 		PWM1_Set_Duty(current_duty);delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main83:
	DECFSZ      R13, 1, 1
	BRA         L_main83
	NOP
;Node2.c,174 :: 		}else if((istenen=='5')&&(akim_sonuc<=650)){
	GOTO        L_main84
L_main82:
	MOVF        _istenen+0, 0 
	XORLW       53
	BTFSS       STATUS+0, 2 
	GOTO        L_main87
	MOVF        _akim_sonuc+1, 0 
	SUBLW       2
	BTFSS       STATUS+0, 2 
	GOTO        L__main139
	MOVF        _akim_sonuc+0, 0 
	SUBLW       138
L__main139:
	BTFSS       STATUS+0, 0 
	GOTO        L_main87
L__main98:
;Node2.c,175 :: 		current_duty++;
	INFSNZ      _current_duty+0, 1 
	INCF        _current_duty+1, 1 
;Node2.c,176 :: 		PWM1_Set_Duty(current_duty); delay_us(200);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       166
	MOVWF       R13, 0
L_main88:
	DECFSZ      R13, 1, 1
	BRA         L_main88
	NOP
;Node2.c,177 :: 		}else if((istenen=='5')&&(akim_sonuc>650)&&(akim_sonuc<900)){
	GOTO        L_main89
L_main87:
	MOVF        _istenen+0, 0 
	XORLW       53
	BTFSS       STATUS+0, 2 
	GOTO        L_main92
	MOVF        _akim_sonuc+1, 0 
	SUBLW       2
	BTFSS       STATUS+0, 2 
	GOTO        L__main140
	MOVF        _akim_sonuc+0, 0 
	SUBLW       138
L__main140:
	BTFSC       STATUS+0, 0 
	GOTO        L_main92
	MOVLW       3
	SUBWF       _akim_sonuc+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main141
	MOVLW       132
	SUBWF       _akim_sonuc+0, 0 
L__main141:
	BTFSC       STATUS+0, 0 
	GOTO        L_main92
L__main97:
;Node2.c,179 :: 		PWM1_Set_Duty(current_duty);delay_ms(10);
	MOVF        _current_duty+0, 0 
	MOVWF       FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
	MOVLW       33
	MOVWF       R12, 0
	MOVLW       118
	MOVWF       R13, 0
L_main93:
	DECFSZ      R13, 1, 1
	BRA         L_main93
	DECFSZ      R12, 1, 1
	BRA         L_main93
	NOP
;Node2.c,180 :: 		}
	GOTO        L_main94
L_main92:
;Node2.c,181 :: 		else if(istenen=='0')
	MOVF        _istenen+0, 0 
	XORLW       48
	BTFSS       STATUS+0, 2 
	GOTO        L_main95
;Node2.c,183 :: 		current_duty=0;
	CLRF        _current_duty+0 
	CLRF        _current_duty+1 
;Node2.c,184 :: 		PWM1_Set_Duty(current_duty);
	CLRF        FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
;Node2.c,185 :: 		}
	GOTO        L_main96
L_main95:
;Node2.c,188 :: 		PWM1_Set_Duty(0);
	CLRF        FARG_PWM1_Set_Duty_new_duty+0 
	CALL        _PWM1_Set_Duty+0, 0
;Node2.c,189 :: 		}
L_main96:
L_main94:
L_main89:
L_main84:
L_main79:
L_main74:
L_main69:
L_main64:
L_main59:
L_main54:
L_main49:
L_main44:
L_main39:
L_main34:
L_main29:
L_main24:
;Node2.c,191 :: 		}
	GOTO        L_main16
;Node2.c,192 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
